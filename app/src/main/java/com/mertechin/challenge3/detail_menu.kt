package com.mertechin.challenge3

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class detail_menu : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_detail_menu)

		// Mengambil data dari halaman sebelumnya
		val data        = intent.getParcelableExtra<data_menu>("DATA")

		// Mengambil id dari activity_detail_menu
		val gambar      = this.findViewById<ImageView>(R.id.img_detail)
		val tvJudul     = this.findViewById<TextView>(R.id.name_detail)
		val tvHarga     = this.findViewById<TextView>(R.id.price_detail)
		val tvDeskripsi = this.findViewById<TextView>(R.id.desc_detail)
		val btnLoc      = this.findViewById<Button>(R.id.btn_detail)

		if (data != null) {
			gambar.setImageResource(data.image)
			tvJudul.text    = data.title
			tvHarga.text    = data.price
			tvDeskripsi.text= data.desc
			btnLoc.text = "Tambah Ke Keranjang - ${data.price}"
		}

		btnLoc.setOnClickListener{
			// Ambil data untuk ditampilkan ke GMaps
			val url = "https://www.google.com/maps/place/Kedai+Ahmed+2/@-7.4546308,112.700934,17z/data=!4m6!3m5!1s0x2dd7e11403db93bd:0xa977fec0db439497!8m2!3d-7.4550085!4d112.7024521!16s%2Fg%2F11qqfbt396?entry=ttu"
			val intent = Intent(Intent.ACTION_VIEW)
			intent.data = Uri.parse(url)
			startActivity(intent)
		}
	}
}