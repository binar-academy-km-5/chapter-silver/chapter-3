package com.mertechin.challenge3

import android.os.Parcel
import android.os.Parcelable

data class data_menu(
	val image: Int,
	val title: String?,
	val price: String?,
	val desc: String?,
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readInt(), parcel.readString(), parcel.readString(), parcel.readString()
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeInt(image)
		parcel.writeString(title)
		parcel.writeString(price)
		parcel.writeString(desc)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<data_menu> {
		override fun createFromParcel(parcel: Parcel): data_menu {
			return data_menu(parcel)
		}

		override fun newArray(size: Int): Array<data_menu?> {
			return arrayOfNulls(size)
		}
	}
}